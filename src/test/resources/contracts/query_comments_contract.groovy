import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method GET()

        urlPath('/comments') {
            queryParameters {
                parameter question: 1
            }
        }

        headers {
            accept applicationJson()
        }

    }

    response {
        status OK()

        headers {
            contentType applicationJson()
        }

        body([
                [
                        id: 1,
                        comment: "Some comment"
                ]
        ])
    }
}