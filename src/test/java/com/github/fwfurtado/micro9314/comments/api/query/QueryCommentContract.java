package com.github.fwfurtado.micro9314.comments.api.query;

import com.github.fwfurtado.micro9314.comments.shared.domains.entities.Comment;
import com.github.fwfurtado.micro9314.comments.shared.domains.entities.CommentStatus;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@ActiveProfiles("integration")
@SpringBootTest
@AutoConfigureTestEntityManager
@Transactional
class QueryCommentContract {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    TestEntityManager entityManager;

    @BeforeEach
    void setup() {
        var comment = new Comment(1L, "Some comment", LocalDateTime.now(), CommentStatus.UNMODERATE);
        entityManager.persist(comment);

        RestAssuredMockMvc.webAppContextSetup(context);
    }
}