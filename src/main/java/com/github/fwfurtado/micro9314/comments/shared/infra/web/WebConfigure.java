package com.github.fwfurtado.micro9314.comments.shared.infra.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

@Configuration
class WebConfigure {

    @Bean
    CommonsRequestLoggingFilter commonsRequestLoggingFilter() {
        var log = new CommonsRequestLoggingFilter();

        log.setIncludeClientInfo(true);
        log.setIncludeHeaders(true);
        log.setIncludePayload(true);
        log.setIncludeHeaders(true);

        return log;
    }

}
