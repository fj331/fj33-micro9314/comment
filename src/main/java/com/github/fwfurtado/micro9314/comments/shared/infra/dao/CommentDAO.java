package com.github.fwfurtado.micro9314.comments.shared.infra.dao;

import com.github.fwfurtado.micro9314.comments.api.query.QueryCommentRepository;
import com.github.fwfurtado.micro9314.comments.api.reply.NewCommentRepository;
import com.github.fwfurtado.micro9314.comments.shared.domains.entities.Comment;
import org.springframework.data.repository.Repository;

interface CommentDAO extends Repository<Comment, Long>, NewCommentRepository, QueryCommentRepository {

}
