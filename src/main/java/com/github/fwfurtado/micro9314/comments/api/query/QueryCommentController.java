package com.github.fwfurtado.micro9314.comments.api.query;

import com.github.fwfurtado.micro9314.comments.api.CommentApi;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@CommentApi
public class QueryCommentController {

    private final QueryCommentRepository repository;

    public QueryCommentController(QueryCommentRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    List<CommentView> listAllCommentsByQuestion(@RequestParam("question") Long questionId) {
        return repository.getAllCommentsByQuestionId(questionId);
    }

    interface CommentView {
        Long getId();
        String getComment();
    }

}
