package com.github.fwfurtado.micro9314.comments.shared.domains.entities;

public enum VoteType {
    UP, DOWN;
}
