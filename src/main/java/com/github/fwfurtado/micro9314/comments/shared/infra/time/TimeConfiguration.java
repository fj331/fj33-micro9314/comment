package com.github.fwfurtado.micro9314.comments.shared.infra.time;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;

@Configuration
class TimeConfiguration {

    @Bean
    Clock clock() {
        return Clock.systemDefaultZone();
    }
}
