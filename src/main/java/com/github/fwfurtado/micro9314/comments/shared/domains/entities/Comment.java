package com.github.fwfurtado.micro9314.comments.shared.domains.entities;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long questionId;

    @NotNull
    private String comment;

    @NotNull
    private LocalDateTime time;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CommentStatus status;

    @ElementCollection
    @CollectionTable(name = "comment_votes", joinColumns = @JoinColumn(name = "comment_id"))
    private List<Vote> votes = new ArrayList<>();

    Comment() {
    }

    public Comment(Long questionId, String comment, LocalDateTime time, CommentStatus status) {
        this.questionId = questionId;
        this.comment = comment;
        this.time = time;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public String getComment() {
        return comment;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public CommentStatus getStatus() {
        return status;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public Long getVoteCounts() {
        var upVotes = votes.stream().map(Vote::getType).filter(VoteType.UP::equals).count();
        var downVotes = votes.stream().map(Vote::getType).filter(VoteType.DOWN::equals).count();

        return upVotes - downVotes;
    }
}
