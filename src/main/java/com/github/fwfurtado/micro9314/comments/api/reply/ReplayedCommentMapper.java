package com.github.fwfurtado.micro9314.comments.api.reply;

import com.github.fwfurtado.micro9314.comments.api.reply.ReplayController.ReplayedComment;
import com.github.fwfurtado.micro9314.comments.shared.domains.entities.Comment;
import com.github.fwfurtado.micro9314.comments.shared.domains.factories.CommentFactory;
import org.springframework.stereotype.Component;

@Component
class ReplayedCommentMapper {
    private final CommentFactory factory;

    ReplayedCommentMapper(CommentFactory factory) {
        this.factory = factory;
    }

    public Comment map(ReplayedComment replayedComment) {
        return factory.factory(replayedComment.questionId(), replayedComment.comment());
    }
}
