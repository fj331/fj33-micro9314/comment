package com.github.fwfurtado.micro9314.comments.api.reply;

import com.github.fwfurtado.micro9314.comments.shared.domains.entities.Comment;

public interface NewCommentRepository {
    void save(Comment comment);
}
