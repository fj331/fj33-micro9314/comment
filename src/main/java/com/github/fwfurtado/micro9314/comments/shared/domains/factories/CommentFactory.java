package com.github.fwfurtado.micro9314.comments.shared.domains.factories;

import com.github.fwfurtado.micro9314.comments.shared.domains.entities.Comment;
import com.github.fwfurtado.micro9314.comments.shared.domains.entities.CommentStatus;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.LocalDateTime;

@Component
public class CommentFactory {

    private final Clock clock;

    public CommentFactory(Clock clock) {
        this.clock = clock;
    }

    public Comment factory(Long questionId, String content) {
        return new Comment(questionId, content, LocalDateTime.now(clock) ,CommentStatus.UNMODERATE);
    }
}
