package com.github.fwfurtado.micro9314.comments.api.reply;

import com.github.fwfurtado.micro9314.comments.api.CommentApi;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import static org.springframework.http.ResponseEntity.created;

@CommentApi
class ReplayController {

    private final ReplyService service;

    ReplayController(ReplyService service) {
        this.service = service;
    }

    @PostMapping
    ResponseEntity<?> replyQuestion(@RequestBody @Validated ReplayedComment comment, UriComponentsBuilder builder)  {
        var id = service.replayQuestionBy(comment);

        var uri = builder.path("comments/{id}").build(id);

        return created(uri).build();
    }

    record ReplayedComment(
            @NotBlank String comment,
            @NotNull @Min(1L) Long questionId) {}
}
