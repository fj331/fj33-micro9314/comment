package com.github.fwfurtado.micro9314.comments.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@RestController
@RequestMapping("comments")
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CommentApi {
}
