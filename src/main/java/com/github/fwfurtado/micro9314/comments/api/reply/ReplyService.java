package com.github.fwfurtado.micro9314.comments.api.reply;

import com.github.fwfurtado.micro9314.comments.api.reply.ReplayController.ReplayedComment;
import org.springframework.stereotype.Service;

@Service
class ReplyService {
    private final ReplayedCommentMapper mapper;
    private final NewCommentRepository repository;

    ReplyService(ReplayedCommentMapper mapper, NewCommentRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }

    public Long replayQuestionBy(ReplayedComment replayedComment) {
        var comment = mapper.map(replayedComment);

        repository.save(comment);

        return comment.getId();
    }
}
