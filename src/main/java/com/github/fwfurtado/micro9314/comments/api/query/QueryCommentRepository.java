package com.github.fwfurtado.micro9314.comments.api.query;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import static com.github.fwfurtado.micro9314.comments.api.query.QueryCommentController.*;

public interface QueryCommentRepository {

    @Query("select c from Comment c where c.questionId = :questionId")
    List<CommentView> getAllCommentsByQuestionId(@Param("questionId") Long questionId);
}
