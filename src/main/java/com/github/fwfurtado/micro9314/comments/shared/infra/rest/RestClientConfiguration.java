package com.github.fwfurtado.micro9314.comments.shared.infra.rest;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients
public class RestClientConfiguration {
}
