package com.github.fwfurtado.micro9314.comments.shared.domains.entities;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Embeddable
public class Vote {
    @NotNull
    private LocalDateTime time;

    @NotNull
    private VoteType type;

    public Vote() {
    }

    public Vote(VoteType type, LocalDateTime time) {
        this.type = type;
        this.time = time;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public VoteType getType() {
        return type;
    }
}
